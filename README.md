```plantuml 
@startmindmap 


# Mapa conceptual "Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)"


```plantuml 
@startmindmap 
*[#C1FFA0] Programación 

 *[#lightblue] Lenguajes
  *_ es el medio de
   * Comunicacion 
    *_ entre
     * Maquina
      *_ y 
       * Hombre
  * Evolucion 
   *_ cambia debido
    * Problemas de programacion
     * Grandes desarrollos
     * Demasiada complejidad
      *_ debido a 
       * Paradigmas no adecuados



  * Paradigma
   *_ es una manera de
    * Abordar un problema
     *_ para encontrar
      * Soluciones
   * Tipos
    * Estructurada
     *_ nos separa de 
      * Dependecias de hardware 
     *_ promueve pensar
      * Alto nivel 
     *_ ofrece una concepcion
      * Diseño modular
     * Ejemplos
      * Basic
      * C
      * Pascal
      * Fortran 

    * Funcional
     *_ se apoya principalmente en
      * Matematicas 
     *_ promueve la
      * Recursividad  
     * Ejemplos
      * ML
      * Haskell
      * Hope  

    * Logico
     *_ se basa en
      * Expresiones logicas 
     *_ objetivo
      * Modelizar problema  
       *_ solucionar mediante
        * Inferencia
     * Ejemplos
      * Prolog

    * POO
     *_ nueva manera de concebir
      * Programacion y la realidad
       *_ a traves de
        * Abstraccion
     *_ soluciona problemas mediante
      * Construir objetos  
       *_ e implementar 
        * Relaciones entre ellos
     * Ejemplos
      * Java
      * C++
      * Python  
     
    * Programacion basada en componentes
     *_ se relaciona con
      * POO
       *_ con el fin
        * Mayor reutilizacion
     * Componente
      *_ es 
       * Conjunto de objetos 
        *_ con mayor
         * Funcionalidad
      
   
 
  @endmindmap 
  ```
















